/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Button,
} from 'react-native';

class Home extends Component {
  render() {
    return (
      <View style={styles.app}>
        <SafeAreaView>
          <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={styles.sectionContainer}>
            <View>
              <Text>Status: Connected</Text>
              <Text>Battery: Good</Text>
            </View>
            <View>
              <View style={styles.sectionContainer}>
                <Button
                  onPress={this.onPressTransfer}
                  title="Transfer"
                  color="black"
                />
                <View style={styles.viewDataButton}>
                  <Button
                    onPress={this.doNothing}
                    title="View Result"
                    color="black"
                  />
                </View>
              </View>
            </View>
          </ScrollView>
        </SafeAreaView>
      </View>
    );
  }

  onPressTransfer = () => {
    const {navigation} = this.props;
    navigation.navigate('Transfer');
  };

  doNothing = () => {
    return;
  };
}

const styles = StyleSheet.create({
  app: {
    flex: 1,
    backgroundColor: 'yellow',
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  viewDataButton: {
    marginTop: 10,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default Home;
